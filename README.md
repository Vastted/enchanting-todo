﻿# [Enchanting to-do](https://enchanting-todo.firebaseapp.com/) main repository
This repository contains source and production [code](https://github.com/Vastted/enchanting-todo/) for the `Enchanting to-do`.

## List used code
* Build with help of [Create React App](https://github.com/facebook/create-react-app) - Create React apps with no build configuration
* [@material-ui](https://material-ui.com) — Components used ([React](https://github.com/facebook/react) only)
* [firebase](https://firebase.google.com) — For registration and database
* [react](https://github.com/facebook/react)
* [redux](https://github.com/reactjs/redux)

## Installation and local launch
  1. Clone this repo: `git clone https://github.com/Vastted/enchanting-todo.git master`
  2. Add [Firebase](https://firebase.google.com) [api](https://firebase.google.com/docs/web/setup) to `source/configuration/api.firebase.js` - `export default {...}`
  3. Run `npm install` in the root folder
  4. Run `npm run start` in the root folder

## License
MIT — use for any purpose. Would be great if you could leave a note about the original developers. Thanks!

## Contributions
Here are indicated the developers and their roles on the project.
  * [Andrey Bondarenko](https://github.com/Vastted) - All development

## Bugs
To send error messages, go to the [link](https://vastted.github.io/portfolio/enchanting-todo/issues/new) and fill in the fields. Thank you.

## Contact Information
  * [Telegram](https://t.me/Vastted)
  * [Twitter](https://twitter.com/Vastted)