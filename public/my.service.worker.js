//───────────────────────────────────-data
let version = 'enchanting.to-do.v=1.2.0';
let expectedCaches = [];
let addToCache = [
    './index.html',
    './manifest.webmanifest',
    './static/styles/main.9bb89100.css',
    './static/scripts/main.6f08e7e5.js',

    './images/icons/check-16x16.png',
    './images/icons/check-32x32.png',
    './images/icons/check-48x48.png',
    './images/icons/check-64x64.png',
    './images/icons/check-96x96.png',
    './images/icons/check-128x128.png',
    './images/icons/check-192x192.png',
    './images/icons/check-256x256.png',
    './images/icons/check-384x384.png',
    './images/icons/check-512x512.png',

    './fonts/Roboto/Roboto-Light.ttf',
    './fonts/Roboto/Roboto-Regular.ttf',
    './fonts/Roboto/Roboto-Medium.ttf'
];
//───────────────────────────────────-data



//───────────────────────────────────-swInstall
self.addEventListener('install', (event) => {
    // console.log('[SW]: install event in progress');
    event.waitUntil(
        caches
            .open(version)
            .then((cache) => cache.addAll(addToCache))
            // .then(() => console.log('[SW]: install completed'))
            .then(() => self.skipWaiting())
            // .then(() => console.log('[SW]: active'))
            .catch((error) => console.error('[SW.Error]:', error))
    );
});
//───────────────────────────────────-swInstall



//───────────────────────────────────-swActivate
self.addEventListener('activate', (event) => {
    // console.log('[SW]: activate event in progress');
    event.waitUntil(
        caches
            .keys()
            .then((keys) => {
                return Promise.all(
                    keys
                        .filter((key) => !key.startsWith(version))
                        .map((key) => {
                            if (!expectedCaches.includes(key)) {
                                return caches.delete(key);
                            }
                            // console.log(`[SW]: remove cache - ${key}`);
                        })
                );
            })
            // .then(() => console.log(`[SW]: ${version} activate completed`))
            .catch((error) => console.error('[SW.Error]:', error))
    );
});
//───────────────────────────────────-swActivate



//───────────────────────────────────-swFetch
self.addEventListener('fetch', (event) => {
    // console.log('[SW]: fetch event in progress');
    if (event.request.method !== 'GET') {
        /*console.log(`[SW]: fetch event ignored\r\n    ${event.request.method}\r\n    ${event.request.url}`);*/
        return;
    }

    event.respondWith(
        caches
            .match(event.request)
            .then((cached) => {
                const fetchedFromNetwork = (response) => {
                    const cacheCopy = response.clone();
                    // console.log(`[SW]: fetch response from network\r\n    ${event.request.url}`);
                    caches
                        .open(version)
                        .then((cache) => cache.put(event.request, cacheCopy))
                        // .then(() => console.log(`[SW]: fetch response stored in cache\r\n    ${event.request.url}`));
                        .catch((error) => console.error('[SW.Error]:', error))

                    return response;
                };

                const unableToResolve = () => {
                    // console.log('[SW]: fetch request failed in both cache and network');
                    // https://codepen.io/xaviersg/pen/rrZoZE
                    return new Response(`<!DOCTYPE html><html lang="en-US" dir="ltr"><head id="head"><title>Enchanting ToDo</title><meta charset="utf-8"><meta http-equiv="x-ua-compatible" content="ie=edge"><meta name="viewport" content="width=device-width, initial-scale=1"><meta name="theme-color" content="#38b0e3"><meta name="HandheldFriendly" content="True"><meta name="title" content="Enchanting ToDo"><meta name="renderer" content="webkit|ie-comp|ie-stand"><meta name="x5-fullscreen" content="true"><meta name="x5-page-mode" content="app"><meta name="full-screen" content="yes"><meta name="browsermode" content="application"><meta name="nightmode" content="disable"><style>@font-face{font-family:'Roboto';font-style:normal;font-weight:300;src:url('/fonts/Roboto/Roboto-Light.ttf') format('truetype')}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;src:url('/fonts/Roboto/Roboto-Regular.ttf') format('truetype')}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;src:url('/fonts/Roboto/Roboto-Medium.ttf') format('truetype')}body{margin:0;font-family:"Roboto","Helvetica","Arial",sans-serif;text-align:center;color:hsla(0, 0%, 0%, 1);background:hsla(0, 0%, 87%, 1);// background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAKklEQVQYV2N89+7dfwYouHfvHgMjTADEUVJSggjAOCCFjGfOnPkPkoEBALUJF8Mcc0jAAAAAAElFTkSuQmCC) repeat;text-align:center}#main{width:100vw;height:100vh;max-width:100vw;max-height:100vh;overflow-x:hidden;text-align:center}.header{display:flex;position:static;width:100%;max-width:100vw;z-index:1100;box-sizing:border-box;flex-shrink:0;flex-direction:column;box-shadow:0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0,0,0,0.12);color:hsla(0,0%,100%,1);background-color:hsla(231,48%,48%,1)}.header-title{display:block;margin:0;flex-grow:1;font-size:1.3125rem;font-weight:500;line-height:1.16667em;text-align:left;user-select:none}.header-toolbar{display:flex;position:relative;align-items:center;width:100%;max-width:100vw;min-height:56px;margin:0 auto;padding-left:16px;padding-right:16px}@media (min-width: 0px) and (orientation: landscape){.header-toolbar{min-height:48px}.header-toolbar{min-height:56px}}@media screen and (min-width: 480px) and (orientation: landscape){.header-toolbar{min-width:480px;width:480px;max-width:480px}.header-toolbar{width:100%;max-width:100vw;margin:0 auto}}@media (min-width: 600px){.header-toolbar{min-height:64px}.header-toolbar{padding-left:24px;padding-right:24px}.header-toolbar{padding-left:16px;padding-right:16px}.header-toolbar{display:flex;position:relative;align-items:center}}@media screen and (min-width: 768px){.header-toolbar{min-width:500px;width:500px;max-width:500px}}.content{max-width:650px;margin:50px auto 0 auto;padding:0 10px;line-height:1.6}.content h2{margin:0;font-size:6rem}.content h3{margin-top:0;margin-bottom:0;font-size:2rem}.content p{margin-top:0;margin-bottom:20px}.content .advice{max-width:400px;margin:0 auto 20px auto;text-align:center}.content a{min-height:36px;padding:8px 16px;font-size:0.875rem;font-weight:500;text-decoration:none;line-height:1.4em;text-transform:uppercase;background:hsla(231, 48%, 48%, 1);color:hsla(0, 0%, 100%, 1);transition:background .25s ease;border:none;border-radius:4px;box-shadow:0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);user-select:none;cursor:pointer}.content a:hover{background:hsla(232, 54%, 41%, 1)}</style></head><body> <main id="main"><header class="header"><div class="header-toolbar"><h2 class="header-title">503</h2></div></header><div class="content"><h2>503</h2><h3>Service temporarly unavailable</h3><p class="advice">The cache is empty and the network is unavailable. To work offline you first need to connect to the network. Sorry for the inconvenience.</p><a href="/">Reload</a></div></main></body></html>`, {
                            status: 503,
                            statusText: 'Service Unavailable',
                            headers: new Headers({
                                'Content-Type': 'text/html'
                            })
                        });
                };



                let networked = fetch(event.request)
                    .then(fetchedFromNetwork, unableToResolve)
                    .catch(unableToResolve);

                // console.log(`[SW]: fetch event ${cached ? '(cached)' : '(network)'}\r\n    ${event.request.url}`);
                return cached || networked;
            })
            .catch((error) => console.error('[SW.Error]:', error))
    );
});
//───────────────────────────────────-swFetch