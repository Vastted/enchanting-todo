//───────────────────────────────────-imports
import { firebase } from './../configuration/firebase.js'
//───────────────────────────────────-imports



//───────────────────────────────────-
export const signIn = (uid, email) => ({ type: 'SIGN_IN', uid, email });
export const signOut = () => ({ type: 'SIGN_OUT' });


export const SignUp = (email, password) => () => firebase.auth().createUserWithEmailAndPassword(email, password);
export const SignIn = (email, password) => () => firebase.auth().signInWithEmailAndPassword(email, password);


export const SignOut = () => () => firebase.auth().signOut();
//───────────────────────────────────-