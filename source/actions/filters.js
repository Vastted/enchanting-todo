//───────────────────────────────────-
export const setTextFilter = (text = '') => (dispatch) => dispatch({ type: 'SET_TEXT_FILTER', text });

export const sortByDate = () => (dispatch) => dispatch({ type: 'SORT_BY_DATE' });
export const sortByTitle = () => (dispatch) => dispatch({ type: 'SORT_BY_TITLE' });

export const filterBy = (value) => (dispatch) => dispatch({ type: 'SET_FILTER', by: value });
//───────────────────────────────────-