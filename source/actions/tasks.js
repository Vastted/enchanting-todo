//───────────────────────────────────-imports
import database from './../configuration/firebase.js';
//───────────────────────────────────-imports



//───────────────────────────────────-addTask
export const addTask = (tasksToDo) => ({ type: 'ADD_TASK', tasksToDo });

export const startAddTask = (taskData = {}) => (dispatch, getState) => {
    const uid = getState().authorization.uid;
    const {
        title = '',
        note = '',
        createdAt = 0,
        completed = false,
        deleted = false
    } = taskData;
    const tasksToDo = { title, note, createdAt, completed, deleted };

    return database.ref(`api/users/${uid}/tasks`)
        .push(tasksToDo)
        .then((reference) => {
            dispatch(addTask({ id: reference.key, ...tasksToDo }))
        })
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-addTask



//───────────────────────────────────-deleteTask
export const deleteTask = ({ id } = {}) => ({ type: 'DELETE_TASK', id });

export const startDeleteTask = ({ id } = {}) => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    // return database.ref(`api/users/${uid}/tasks/${id}`).remove()
    return database.ref(`api/users/${uid}/tasks/${id}`).update({ 'deleted': true })
        .then(() => dispatch(deleteTask({ id })))
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-deleteTask

//───────────────────────────────────-unDeleteTask
export const unDeleteTask = ({ id } = {}) => ({ type: 'UN_DELETE_TASK', id });

export const startUnDeleteTask = ({ id } = {}) => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    return database.ref(`api/users/${uid}/tasks/${id}`).update({ 'deleted': false })
        .then(() => dispatch(unDeleteTask({ id })))
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-unDeleteTask

//───────────────────────────────────-deleteAll
export const deleteAll = () => ({ type: 'DELETE_ALL' });

export const startDeleteAll = () => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    const allTasks = getState().tasksToDo;
    for (let i = 0; i < allTasks.length; i++) {
        database.ref(`api/users/${uid}/tasks/${allTasks[i].id}`).update({ 'deleted': true })
    }

    // return database.ref(`api/users/${uid}/tasks`).remove()
    return database.ref(`api/users/${uid}/data`).update({ 'delete_all': true })
        .then(() => dispatch(deleteAll()))
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-deleteAll



//───────────────────────────────────-editTask
export const editTask = (id, updates) => ({ type: 'EDIT_TASK', id, updates });

export const startEditTask = (id, updates) => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    return database.ref(`api/users/${uid}/tasks/${id}`).update(updates)
        .then(() => dispatch(editTask(id, updates)))
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-editTask



//───────────────────────────────────-completedTask
export const completedTask = ({ id } = {}) => ({ type: 'SET_TO_COMPLETE', id });

export const startCompletedTask = ({ id } = {}) => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    return database.ref(`api/users/${uid}/tasks/${id}`).update({ 'completed': true })
        .then(() => dispatch(completedTask({ id })))
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-completedTask

//───────────────────────────────────-uncompletedTask
export const uncompletedTask = ({ id } = {}) => ({ type: 'SET_TO_UNCOMPLETED', id });

export const startUnCompletedTask = ({ id } = {}) => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    return database.ref(`api/users/${uid}/tasks/${id}`).update({ 'completed': false })
        .then(() => dispatch(uncompletedTask({ id })))
        .catch((error) => console.log(`Something went wrong: ${error}`))
};
//───────────────────────────────────-uncompletedTask

//───────────────────────────────────-completeAll
export const completeAll = (tasksToDo) => ({ type: 'COMPLETE_ALL', tasksToDo });

export const startCompleteAll = () => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    const allTasks = getState().tasksToDo;
    for (let i = 0; i < allTasks.length; i++) {
        allTasks[i].completed = true;
        database.ref(`api/users/${uid}/tasks/${allTasks[i].id}`).update({ 'completed': true });
    }

    return database.ref(`api/users/${uid}/data`).update({ 'complete_all': true })
        .then(() => dispatch(completeAll(allTasks)))
        .catch((error) => console.log(`Something went wrong: ${error}`));
};
//───────────────────────────────────-completeAll



//───────────────────────────────────-setTasks
export const setTasks = (tasksToDo) => ({ type: 'SET_TASKS', tasksToDo });

export const startSetTasks = () => (dispatch, getState) => {
    const uid = getState().authorization.uid;

    return database.ref(`api/users/${uid}/tasks`).once('value')
        .then((snapshot) => {
            const tasksToDo = [];

            snapshot.forEach((childSnapshot) => {
                tasksToDo.push({ id: childSnapshot.key, ...childSnapshot.val() })
            });

            dispatch(setTasks(tasksToDo));
        });
};
//───────────────────────────────────-setTasks