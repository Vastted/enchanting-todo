//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { SignOut } from './../../../actions/authorization.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-CustomAppBar
const CustomAppBar = ({ title, isSignIn, dispatch }) => (
    <div className="custom-app-bar_container" >
        <AppBar position="static">
            <Toolbar className="custom-app-bar_toolbar">
                <Typography variant="title" color="inherit">
                    {title}
                </Typography>

                {isSignIn ? (
                    <Button
                        className="custom-app-bar_toolbar_button"
                        color="inherit"
                        onClick={() => dispatch(SignOut())}
                    >Sign Out</Button>
                ) : (
                    <Button
                        className="custom-app-bar_toolbar_link"
                        color="inherit"
                        component={Link}
                        to="/signin"
                    >Sign In</Button>
                )}
            </Toolbar>
        </AppBar>
    </div>
);

const mapStateToProps = (state) => ({
    isSignIn: !!state.authorization.uid
});


export default connect(mapStateToProps)(CustomAppBar);
//───────────────────────────────────-CustomAppBar