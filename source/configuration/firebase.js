//───────────────────────────────────-imports
import * as firebase from 'firebase';

import firebaseApi from './api.firebase.js';
//───────────────────────────────────-imports



//───────────────────────────────────-
firebase.initializeApp(firebaseApi);
const database = firebase.database();


export { firebase, database as default };
//───────────────────────────────────-