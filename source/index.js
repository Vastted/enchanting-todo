//───────────────────────────────────-imports
import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configureStore from './store/configureStore.js';

import ApplicationRouter, { history } from './routers/ApplicationRouters.js'

import { Router, Route, Switch } from 'react-router-dom';
import Offline from './pages/Offline/index.js';

import { startSetTasks } from './actions/tasks.js'
import { signIn, signOut } from './actions/authorization.js'
import { firebase } from './configuration/firebase.js'

import { register } from './modules/sw.manager.js';

import './styles/main.css';
//───────────────────────────────────-imports



//───────────────────────────────────-main
const store = configureStore();
let hasRendered = false;

const applicationTemplate = (
    <Provider store={store}>
        <ApplicationRouter />
    </Provider>
);


const renderApplication = () => {
    if (!hasRendered) {
        ReactDOM.render(applicationTemplate, document.querySelector('#main'));
        hasRendered = true;
    }
};


if (!navigator.onLine) {
    ReactDOM.render(
        <Provider store={store}>
            <Router history={history}>
                <div>
                    <Switch>
                        <Route component={Offline} />
                    </Switch>
                </div>
            </Router>
        </Provider>,

        document.querySelector('#main')
    );
}


firebase.auth().onAuthStateChanged((user) => {
    if (user) {
		store.dispatch(signIn(user.uid, user.email));
        store.dispatch(startSetTasks()).then(() => {
            renderApplication();

            if (history.location.pathname === '/signin') {
                history.push('/todos');
            }
        });
    } else {
		store.dispatch(signOut());
        renderApplication();
        history.push('/');
    }
});



register();
//───────────────────────────────────-main