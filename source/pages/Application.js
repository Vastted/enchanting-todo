//───────────────────────────────────-imports
import React from 'react';

// import ConnectedTasks from './../components/modules/Tasks/index.js';
import ConnectedTasks from './Tasks/index.js';
//───────────────────────────────────-imports


//───────────────────────────────────-Application
const Application = () => <ConnectedTasks />;


export default Application;
//───────────────────────────────────-Application