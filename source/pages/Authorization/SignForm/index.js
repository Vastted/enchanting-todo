//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-SignForm
class SignForm extends Component {
    state = {
        email: '',
        password: '',

        loading: false,
        error: null,

        emailValid: false,
        passwordValid: false,
        formValid: false
    }

    componentDidMount() {
        this.props.onReference(this);
    }

    componentWillUnmount() {
        this.props.onReference(undefined);
    }

    onSubmit = (event) => {
        event.preventDefault();
        const { email, password } = this.state;
        const { dispatch, onSubmitMethod } = this.props;

        if (!email || !password) {
            this.setState(() => ({
                error: 'Fill in all the fields'
            }));
        } else {
            dispatch(onSubmitMethod(email, password))
                .then(() => {
                    this.onSignSuccess();
                })
                .catch(() => this.onSignFailure());
        }
    }

    handleChange = (event) => {
        const { type, value } = event.target;

        this.setState({
            [type]: value
        });


        type === 'email'
            ? this.setState({
                emailValid: value.trim().match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
            })
            : this.setState({
                passwordValid: value.trim().length >= 4
            });

        this.setState({
            formValid: (this.state.emailValid
                && this.state.passwordValid)
        });
    }

    onSignFailure() {
        this.setState({
            loading: false,
            error: this.props.errorText
        });
    }

    onSignSuccess() {
        this.setState({
            loading: false,
            email: '',
            password: ''
        });
    }

    render() {
        const {
            email,
            password,
            error,
            loading,
            emailValid,
            passwordValid,
            formValid
        } = this.state;

        return (
            <form
                onSubmit={this.onSubmit}
                noValidate
                className="sign-form_container"
                autoComplete="on"
            >
                <TextField
                    required
                    error={!emailValid}
                    label="Email"
                    type="email"
                    value={email}
                    onChange={this.handleChange}
                    margin="normal"
                    className="sign-form_text-field"
                />
                <TextField
                    required
                    error={!passwordValid}
                    label="Password"
                    type="password"
                    value={password}
                    onChange={this.handleChange}
                    margin="normal"
                    className="sign-form_text-field"
                />
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={loading || !formValid}
                    className="sign-form_submit-button"
                >
                    {loading ? 'Loading...' : this.props.buttonTitle}
                </Button>

                {error &&
                    <div className="sign-form_error-text">
                        <p style={{ color: 'red' }}>{error}</p>
                    </div>
                }
            </form>
        );
    }
};


export default connect()(SignForm);
//───────────────────────────────────-SignForm