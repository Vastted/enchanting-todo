//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';

import SignForm from './../../SignForm/index.js';

import { SignIn } from './../../../../actions/authorization.js';
//───────────────────────────────────-imports



//───────────────────────────────────-SignInForm
// Variant (with this.props.onRef(this)) - https://github.com/kriasoft/react-starter-kit/issues/909
const SignInForm = () => (
    <SignForm
        onReference={(reference) => (this.child = reference)}

        errorText="Authentication failed"
        buttonTitle="Sign In"
        onSubmitMethod={SignIn}

    />
);

/* class SignInForm extends SignForm {
    errorText = 'Authentication failed'
    buttonTitle = 'Sign In'

    onSubmit = (event) => {
        event.preventDefault();

        const { email, password } = this.state;
        const { dispatch } = this.props;

        if (!email || !password) {
            this.setState(() => ({
                error: 'Fill in all the fields'
            }));
        } else {
            dispatch(SignIn(email, password))
                .then(() => {
                    this.onSignSuccess();
                })
                .catch(() => this.onSignFailure());
        }
    }
}; */


/* class SignInForm extends Component {
    state = {
        email: '',
        password: '',

        loading: false,
        error: null,

        emailValid: true,
        passwordValid: true,
        formValid: false,

        validateNumber: 1
    }

    validateForm() {
        this.setState({
            formValid: (this.state.validateNumber === 1
                        ? false
                        : (this.state.emailValid
                            && this.state.passwordValid)),
            validateNumber: this.state.validateNumber + 1
        });
    }

    validateField = (name, value) => {
        let { emailValid, passwordValid } = this.state;

        switch (name) {
            case 'email':
                emailValid = value.trim().match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                break;
            case 'password':
                passwordValid = value.trim().length >= 4;
                break;
            default:
                break;
        }

        this.setState({
            emailValid,
            passwordValid
        });

        this.validateForm();
    }

    handleChange = (event) => {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        });

        this.validateField(name, value);
    }

    onSubmit = (event) => {
        event.preventDefault();

        const { email, password } = this.state;
        const { dispatch } = this.props;

        if (!email || !password) {
            this.setState(() => ({
                error: 'Fill in all the fields'
            }));
        } else {
            dispatch(SignIn(email, password))
                .then(() => {
                    this.onLoginSuccess();
                })
                .catch(() => this.onLoginFailure());
        }
    }

    onLoginFailure() {
        this.setState({
            loading: false,
            error: 'Authentication failed'
        });
    }

    onLoginSuccess() {
        this.setState({
            loading: false,
            email: '',
            password: ''
        });
    }

    render() {
        const {
            email,
            password,
            error,
            loading,
            emailValid,
            passwordValid,
            formValid
        } = this.state;

        return (
            <form
                onSubmit={this.onSubmit}
                noValidate
                autoComplete="off">

                <TextField
                    required
                    error={!emailValid}
                    label="Email"
                    type="email"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                    margin="normal"
                />
                <TextField
                    required
                    error={!passwordValid}
                    label="Password"
                    type="password"
                    name="password"
                    value={password}
                    onChange={this.handleChange}
                    margin="normal"
                />
                {loading ? (
                    <div className="center-text">
                        Loading...
                    </div>
                ) : (
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        disabled={!formValid}
                    >
                        Sign In
                    </Button>
                )}

                {error &&
                    <div className="error-text">
                        <p style={{ color: 'red' }}>{error}</p>
                    </div>
                }
            </form>
        );
    }
}; */


export default connect()(SignInForm);
//───────────────────────────────────-SignInForm