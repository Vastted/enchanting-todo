//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';

import CustomAppBar from './../../../components/common/CustomAppBar/index.js';
import SignInForm from './SignInForm/index.js';
//───────────────────────────────────-imports



//───────────────────────────────────-SignIn
const SignIn = () => (
    <div className="sign-in_container">
        <CustomAppBar title="Sign In" />

        <SignInForm />
    </div>
);


export default connect()(SignIn);
//───────────────────────────────────-SignIn