//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';

import SignForm from './../../SignForm/index.js';

import { SignUp } from './../../../../actions/authorization.js';
//───────────────────────────────────-imports



//───────────────────────────────────-SignUpForm
// Variant (with this.props.onRef(this)) - https://github.com/kriasoft/react-starter-kit/issues/909
const SignUpForm = () => (
    <SignForm
        onReference={(reference) => (this.child = reference)}

        errorText="Sign Up failed"
        buttonTitle="Sign Up"
        onSubmitMethod={SignUp}
    />
);

/* class SignUpForm extends SignForm {
    errorText = 'Sign Up failed'
    buttonTitle = 'Sign Up'

    onSubmit = (event) => {
        event.preventDefault();

        const { email, password } = this.state;
        const { dispatch } = this.props;

        if (!email || !password) {
            this.setState(() => ({
                error: 'Fill in all the fields'
            }));
        } else {
            dispatch(SignUp(email, password))
                .then(() => {
                    this.onSignSuccess();
                })
                .catch(() => this.onSignFailure());
        }
    }
}; */


/* class SignUpForm extends Component {
    state = {
        email: '',
        password: '',
        loading: false,
        error: null
    }

    handleChange = (event) => this.setState({
        [event.target.name]: event.target.value
    })

    onSubmit = (event) => {
        event.preventDefault();

        const { email, password } = this.state;
        const { dispatch } = this.props;

        if (!email || !password) {
            this.setState(() => ({
                error: 'Fill in all the fields'
            }));
        } else {
            dispatch(SignUp(email, password))
                .then(() => {
                    this.onSignUpSuccess();
                })
                .catch(() => this.onSignUpFailure());
        }
    }

    onSignUpFailure() {
        this.setState({
            loading: false,
            error: 'Sign up failed'
        });
    }

    onSignUpSuccess() {
        this.setState({ 
            loading: false,
            email: '',
            password: ''
        });
    }

    render() {
        const { email, password, error, loading } = this.state;

        return (
            <form onSubmit={this.onSubmit}>
                <div className="left-text login-inputs">
                    <CustomInput
                        icon="email"
                        type="email"
                        name="email"
                        placeholder="Enter your email"
                        autoComplete="on"
                        value={email}
                        onChange={this.handleChange}
                    />
                    <CustomInput
                        icon="password"
                        type="password"
                        name="password"
                        placeholder="Enter your password"
                        autoComplete="on"
                        value={password}
                        onChange={this.handleChange}
                    />
                </div>
                {error &&
                    <div className="center-text">
                        <p style={{ color: 'red' }}>{error}</p>
                    </div>
                }
                {loading ? (
                    <div className="center-text">
                        Loading...
                    </div>
                ) : (
                    <div className="login-btn">
                        <CustomButton typeBtn="submit">Sign Up</CustomButton>
                    </div>
                )}
            </form>
        );
    }
}; */


export default connect()(SignUpForm);
//───────────────────────────────────-SignUpForm