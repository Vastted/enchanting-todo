//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';

import CustomAppBar from './../../../components/common/CustomAppBar/index.js';
import SignUpForm from './SignUpForm/index.js';
//───────────────────────────────────-imports



//───────────────────────────────────-SignIn
const SignIn = () => (
    <div className="sign-up_container">
        <CustomAppBar title="Sign Up" />

        <SignUpForm />
    </div>
);


export default connect()(SignIn);
//───────────────────────────────────-SignIn