//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import CustomAppBar from './../../components/common/CustomAppBar/index.js';

import Button from '@material-ui/core/Button';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Home
const Home = () => (
    <div className="home_container">
        <CustomAppBar title="Home" />

        <section>
            <h1>This is awesome todo application</h1>
            <p>
                Please
                <Button
                    variant="contained"
                    component={Link}
                    to="/signin"
                >Sign In</Button>
                or
                <Button
                    variant="contained"
                    color="primary"
                    component={Link}
                    to="/signup"
                >Sign Up</Button>
            </p>
        </section>
    </div>
);


export default connect()(Home);
//───────────────────────────────────-Home