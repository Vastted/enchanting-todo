//───────────────────────────────────-imports
import React from 'react';
import { Link } from 'react-router-dom';

import CustomAppBar from './../../components/common/CustomAppBar/index.js';
import Button from '@material-ui/core/Button';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Information
const Information = ({ title, description, advice, buttonText }) => (
    <div className="information-page_container">
        <CustomAppBar title={title} />

        <section className="information-page_content">
            <h2>{title}</h2>
            <h3>{description}</h3>
            <p class="advice">{advice}</p>
            <Button
                className="information-page_button"
                color="primary"
                variant="contained"
                component={Link}
                to="/"
            >{buttonText}</Button>
        </section>
    </div>
);


export default Information;
//───────────────────────────────────-Information