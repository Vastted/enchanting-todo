//───────────────────────────────────-imports
import React from 'react';

import Information from './../Information/index.js';
//───────────────────────────────────-imports



//───────────────────────────────────-Notfound
const Notfound = () => (
    <div className="not-found_container">
        <Information
            title="404"
            description="Page not found"
            advice="This page does not exist or has been deleted. Please, check the correctness of the path. We apologize for the inconvenience."
            buttonText="Go back Home"
        />
    </div>
);


export default Notfound;
//───────────────────────────────────-Notfound