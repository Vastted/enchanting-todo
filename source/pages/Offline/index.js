//───────────────────────────────────-imports
import React from 'react';

import Information from './../Information/index.js';
//───────────────────────────────────-imports



//───────────────────────────────────-Offline
const Offline = () => (
    <div className="offline_container">
        <Information
            title="Offline"
            description="You're not online"
            advice="This page needs an internet connection because it uses the services of Firebase to authorize and store data. Please, check your connection. We apologize for the inconvenience."
            buttonText="Reload"
        />
    </div>
);


export default Offline;
//───────────────────────────────────-Offline