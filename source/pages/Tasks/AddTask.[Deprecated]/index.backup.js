//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { startAddTask } from './../../../actions/tasks.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-AddTask
class AddTask extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
    
            title: props.task ? props.task.title : '',
            note: props.task ? props.task.note : '',
            createdAt: props.task ? props.task.createdAt : Date.parse(new Date()),
            completed: props.task ? props.task.completed : false,
            deleted: props.task ? props.task.deleted : false,
            error: undefined
        };
    }

    handleChange = (event) => {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        })
    }

    onSubmit = () => {
        const { title, note, createdAt, completed, deleted } = this.state;
        const onSubmit = (task) => {
            const { dispatch, history } = this.props;
            dispatch(startAddTask(task, history));
        };

        if (!title) {
            this.setState({ error: 'Fill in all the fields' });
        } else {
            onSubmit({
                title,
                note,
                createdAt: createdAt.valueOf(),
                completed,
                deleted
            });
        }
        
        this.handleToggleDialog();
    }

    handleToggleDialog = () => {
        this.setState((state) => ({
            open: !state.open
        }));
    };

    render() {
        return (
            <div className="tasks_add-container">
                <Button
                    variant="fab"
                    aria-label="Add new task"
                    onClick={this.handleToggleDialog}
                    className="tasks_button-add"
                >
                    <AddIcon />
                </Button>

                <Dialog
                    open={this.state.open}
                    onClose={this.handleToggleDialog}
                    aria-labelledby="tasks_dialog-title"
                >
                    <DialogTitle id="tasks_dialog-title">Add new task</DialogTitle>
                    <DialogContentText className="tasks_dialog-content-text">
                        To add new task, please enter task title here. You can always change it by double-clicking on it.
                    </DialogContentText>
                    <DialogContent>
                        <TextField
                            required
                            autoComplete="off"
                            autoFocus
                            label="Task title"
                            type="text"
                            name="title"
                            fullWidth
                            margin="dense"
                            onChange={this.handleChange}
                        />
                        <TextField
                            autoComplete="off"
                            label="Task note"
                            type="text"
                            name="note"
                            fullWidth
                            margin="dense"
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleToggleDialog}>
                            Cancel
                        </Button>
                        <Button onClick={this.onSubmit} color="primary">
                            Add task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
};


export default connect()(AddTask);
//───────────────────────────────────-AddTask