//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';

import TaskForm from './../TaskForm/index.js';

import { startAddTask } from './../../../actions/tasks.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-AddTask
class AddTask extends Component {
    render() {
        return (
            <div className="tasks_add-container">
                <Button
                    variant="fab"
                    aria-label="Add new task"
                    onClick={() => {
                        this.child.handleToggleDialog();
                    }}
                    className="tasks_button-add"
                >
                    <AddIcon />
                </Button>

                <TaskForm
                    dialogTitle="Add new task"
                    dialogText="To add new task, please enter task title here. You can always change it by double-clicking on it."
                    defaultTitleValue=""
                    defaultNoteValue=""
                    buttonConfirmText="Add task"
                    onReference={(reference) => (this.child = reference)}
                    onSubmit={(task) => {
                        const { dispatch } = this.props;
                        dispatch(startAddTask(task));
                    }}
                />
            </div>
        );
    }
};


export default connect()(AddTask);
//───────────────────────────────────-AddTask