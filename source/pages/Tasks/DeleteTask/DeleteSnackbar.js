//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Snackbar from '@material-ui/core/Snackbar';

import { startUnDeleteTask } from './../../../actions/tasks.js';
//───────────────────────────────────-imports



//───────────────────────────────────-DeleteSnackbar
class DeleteSnackbar extends Component {
    state = {
        snackbarOpen: false,
        
        vertical: 'bottom',
        horizontal: 'left'
    }

    handleToggleSnackbar = () => {
        this.setState((state) => ({
            snackbarOpen: !state.snackbarOpen
        }));
    }

    render() {
        const {
            snackbarOpen,

            vertical,
            horizontal
        } = this.state;

        return (
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={snackbarOpen}
                onClose={this.handleToggleSnackbar}
                ContentProps={{
                    'aria-describedby': 'delete-task_message',
                }}
                className="delete-task_snackbar"
                message={(
                    <span className="delete-task_message-wrapper">
                        <span className="delete-task_message">The task was deleted</span>
                        <Button
                            color="primary"
                            // className="delete-task_message_undo-button"
                            onClick={() => {
                                dispatch(startUnDeleteTask({ id: taskID }));
                                this.handleToggleSnackbar();
                            }}
                        >
                            Restore
                            </Button>
                    </span>
                )}
            />
        );
    }
};


export default connect()(DeleteSnackbar);
//───────────────────────────────────-DeleteSnackbar