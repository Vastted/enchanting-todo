//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import TaskDialog from './../TaskDialog/index.js';

import { startDeleteTask } from './../../../actions/tasks.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-DeleteTask
class DeleteTask extends Component {
    render() {
        const {
            dispatch,
            taskID,

            expanded
        } = this.props;

        return (
            <div className="tasks_delete-container">
                <IconButton
                    aria-label="Delete"
                    color="secondary"
                    className={`tasks_task-delete-icon ${expanded ? 'expanded' : ''}`}
                >
                    <DeleteIcon onClick={() => {
                        this.deleteTaskChild.handleToggleDialog();
                    }} />
                </IconButton>

                <TaskDialog
                    dialogTitle="Delete task"
                    contentText="Are you sure you want to delete this task?"
                    buttonNoText="No, leave task"
                    buttonYesText="Yes, delete task"
                    onReference={(reference) => (this.deleteTaskChild = reference)}
                    onClickYes={() => {
                        dispatch(startDeleteTask({ id: taskID }));
                    }}
                />
            </div>
        );
    }
};


export default connect()(DeleteTask);
//───────────────────────────────────-DeleteTask