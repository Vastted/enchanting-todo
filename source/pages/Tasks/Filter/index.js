//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { filterBy } from './../../../actions/filters.js';

import './style.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Filter
class Filter extends Component {
    state = {
        value: 0,
    }

    handleChange = (event, value) => {
        this.setState({ value });
    }

    render() {
        const { filterBy, filters } = this.props;

        return (
            <Tabs
                value={this.state.value}
                indicatorColor="primary"
                textColor="primary"
                onChange={this.handleChange}
                className="tasks_filter-container"
            >
                {['All', 'Active', 'Done'].map((item, index) =>
                    <Tab
                        className={`${filters.show === item ? 'tasks_filter-active' : ''}`}
                        key={index}
                        onClick={() => filterBy(item)}
                        label={item}
                    >
                    </Tab>
                )}
            </Tabs>
        );
    }
};

const mapStateToProps = ({ filters }) => ({
    filters
});


export default connect(mapStateToProps, { filterBy })(Filter);
//───────────────────────────────────-Filter