//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
//───────────────────────────────────-imports



//───────────────────────────────────-Separation
class Separation extends Component {
    state = {
        pager: {}
    }

    componentWillMount() {
        if (this.props.items && this.props.items.length) {
            this.setPage(this.props.initialPage);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.items !== prevProps.items) {
            this.setPage(this.props.initialPage);
        }
    }

    setPage(page) {
        let { items, pageSize } = this.props;
        let pager = this.state.pager;

        if (page < 1 || page - 1 > pager.totalPages) {
            // return;
            page = pager.totalPages || 1;
        }

        pager = this.getPager(items.length, page, pageSize);
        let pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

        if (pageOfItems.length === 0) {
            if (page === 1) {} else {
                return this.setPage(page - 1);
            }
        }

        this.setState({ pager: pager });
        this.props.onChangePage(pageOfItems, page);
    }

    getPager(totalItems, currentPage, pageSize) {
        currentPage = currentPage || 1;
        pageSize = pageSize || 5;
        // pageSize = 1000000;
        let pageValue = (window.innerWidth || document.body.clientWidth) < 390
            ? 1
            : 2;

        let totalPages = Math.ceil(totalItems / pageSize);
        let startPage, endPage;

        if (totalPages <= 3) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currentPage <= 2) {
                startPage = 1;
                endPage = (pageValue === 2 ? 5 : 3); // Max pagination buttons 5 - desctop, 3 mobile
            } else if (currentPage + 1 >= totalPages) {
                startPage = totalPages - pageValue - (pageValue === 2 ? 2 : 1); // Max pagination buttons 2 - desctop, 1 mobile
                endPage = totalPages;
            } else {
                startPage = currentPage - pageValue;
                endPage = currentPage + pageValue;
            }
        }

        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        let pages = [...Array((endPage + 1) - startPage).keys()].map((i) => startPage + i);

        return {
            totalItems,
            currentPage,
            pageSize,
            totalPages,
            startPage,
            endPage,
            startIndex,
            endIndex,
            pages
        };
    }

    render() {
        let pager = this.state.pager;

        if (!pager.pages || pager.pages.length <= 1) {
            return null;
        }

        return (
            <nav className="separation-pagination">
                <ul>
                    {/* <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                        <a onClick={() => this.setPage(1)}>First</a>
                    </li> */}
                    {/* <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                        <a onClick={() => this.setPage(pager.currentPage - 1)}>Previous</a>
                    </li> */}
                    {pager.pages.map((page, index) =>
                        <li key={index}>
                            <Button
                                variant="contained"
                                color={pager.currentPage === page ? 'primary' : 'default'}
                                onClick={() => this.setPage(page)}
                            >{page}
                            </Button>
                        </li>
                    )}
                    {/* <li className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
                        <a onClick={() => this.setPage(pager.currentPage + 1)}>Next</a>
                    </li> */}
                    {/* <li className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
                        <a onClick={() => this.setPage(pager.totalPages)}>Last</a>
                    </li> */}
                </ul>
            </nav>
        );
    }
};


export default connect()(Separation);
//───────────────────────────────────-Separation