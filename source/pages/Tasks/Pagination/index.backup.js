//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import SelectedTasks from './../Selectors/index.js';
import Task from './../Task/index.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Pagination
class Pagination extends Component {
    render() {
        const { tasks, filters } = this.props;

        return (
            <div className="tasks_wrapper">
                {tasks.filter((task) => {
                    switch (filters.show) {
                        case 'All':
                            return task;
                        case 'Active':
                            return !task.completed;
                        case 'Done':
                            return !!task.completed;
                        default:
                            return task;
                    }
                }).map((task) => <Task key={task.id} {...task} />)}
            </div >
        );
    }
};

const ConnectedTasks = ({ tasksToDo, filters }) => ({
    tasks: SelectedTasks(tasksToDo, filters),
    filters
});


export default connect(ConnectedTasks)(Pagination);
//───────────────────────────────────-Pagination