//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Separation from './Separation.js';
import SelectedTasks from './../Selectors/index.js';
import Task from './../Task/index.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Pagination
class Pagination extends Component {
    constructor(props) {
        super(props);

        const { filters, tasks } = this.props;

        this.state = {
            tasks,
            toSendTasks: tasks.filter((task) => {
                switch (filters.show) {
                    case 'All':
                        return task;
                    case 'Active':
                        return !task.completed;
                    case 'Done':
                        return !!task.completed;
                    default:
                        return task;
                }
            }),
            pageOfItems: [],
            currentPage: 1,
            isExtendedTask: {}
        };

        this.onChangePage = this.onChangePage.bind(this);
        this.changeExpandedTask = this.changeExpandedTask.bind(this);
    }

    // componentDidUpdate(prevProps) { // React 17
    componentWillReceiveProps(nextProps) {
        const { filters } = nextProps;

        this.setState({
            toSendTasks: nextProps.tasks.filter((task) => {
                switch (filters.show) {
                    case 'All':
                        return task;
                    case 'Active':
                        return !task.completed;
                    case 'Done':
                        return !!task.completed;
                    default:
                        return task;
                }
            })
        });
    }

    onChangePage(pageOfItems, currentPage) {
        this.setState({ pageOfItems, currentPage });
    }

    /* getPageSize = () => {
        const screenHeight = (window.innerHeight || document.body.clientHeight);

        return (screenHeight < 570
            ? 5
            : (screenHeight < 670)
                ? 7
                : (screenHeight < 830)
                    ? 8
                    : 15);
    } */

    changeExpandedTask = (taskID, expanded) => {
        let extendedTaskCopy = JSON.parse(JSON.stringify(this.state.isExtendedTask));
        extendedTaskCopy[taskID] = expanded;

        this.setState({
            isExtendedTask: extendedTaskCopy
        });
    }

    render() {
        const isOneTaskExpandend = (objectOfTasks) => Object.values(objectOfTasks).includes(true);

        return (
            <div className={`tasks_wrapper ${isOneTaskExpandend(this.state.isExtendedTask) ? 'active' : ''}`}>
                {this.state.pageOfItems.map((task) => <Task onExpanded={this.changeExpandedTask} key={task.id} {...task} />)}

                <Separation
                    items={this.state.toSendTasks}
                    onChangePage={this.onChangePage}
                    initialPage={this.state.currentPage}
                    // initialPage={1}
                    // pageSize={this.getPageSize() || 5}
                    pageSize={5}
                />
            </div>
        );
    }
};

const ConnectedTasks = ({ tasksToDo, filters }) => ({
    tasks: SelectedTasks(tasksToDo, filters),
    filters
});


export default connect(ConnectedTasks)(Pagination);
//───────────────────────────────────-Pagination