//───────────────────────────────────-
export default (tasks, { text, sortBy }) => {
    return tasks.filter((task) => {
        if (task.deleted) {
            return false;
        } else {
            const textMatch = task.title.toLowerCase().includes(text.toLowerCase());
            return textMatch;
        }
    }).sort((first, second) => {
        switch (sortBy) {
            case 'date':
                return first.createdAt < second.createdAt ? 1 : -1;
            case 'title':
                return first.title > second.title ? 1 : -1;
            default:
                return first.createdAt < second.createdAt ? 1 : -1;
        }
    });
};
//───────────────────────────────────-