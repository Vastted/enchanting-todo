//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { setTextFilter, sortByDate, sortByTitle } from './../../../actions/filters.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-SortBy
class SortBy extends Component {
    state = {
        sortBy: 'date',
        sortText: ''
    }

    onChangeSelect = (event) => {
        const {
            sortByDate,
            sortByTitle
        } = this.props;

        const { value } = event.target;

        value === 'date'
            ? sortByDate()
            : sortByTitle();

        this.setState({
            sortBy: value
        });
    }

    onChangeText = (event) => {
        const {
            setTextFilter
        } = this.props;

        const { value } = event.target;

        setTextFilter(value);

        this.setState({
            sortText: value
        });
    }

    render() {
        return (
            <div className="sort-by_container">
                <TextField
                    type="text"
                    label="Search for a task"
                    value={this.state.sortText}
                    onChange={this.onChangeText}
                    margin="normal"
                />

                <FormControl className="sort-by_form-control-container">
                    <InputLabel htmlFor="sort-by_simple">Sort by</InputLabel>
                    <Select
                        value={this.state.sortBy}
                        onChange={this.onChangeSelect}
                        inputProps={{
                            name: 'sort-by',
                            id: 'sort-by_simple',
                        }}
                    >
                        <MenuItem value="" disabled>
                            Sort by
                        </MenuItem>
                        <MenuItem value="date">Date</MenuItem>
                        <MenuItem value="title">Title</MenuItem>
                    </Select>
                </FormControl>
            </div>
        );
    }
};

const ConnectedFilter = ({ filters }) => ({
    filters
});


export default connect(ConnectedFilter, {
    setTextFilter,
    sortByDate,
    sortByTitle
})(SortBy);
//───────────────────────────────────-SortBy