//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';

import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';

import TaskDialog from './../TaskDialog/index.js';
import TaskForm from './../TaskForm/index.js';

import { startAddTask, startCompleteAll, startDeleteAll } from './../../../actions/tasks.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-SpeedDialTask
class SpeedDialTask extends Component {
    state = {
        speedDialOpen: false
    }

    handleClickSpeedDial = () => {
        this.setState((state) => ({
            speedDialOpen: !state.speedDialOpen
        }));
    }

    render() {
        const { dispatch } = this.props;
        const { speedDialOpen } = this.state;

        /* let isTouch;
        if (typeof document !== 'undefined') {
            isTouch = 'ontouchstart' in document.documentElement;
        } */

        return (
            <div className={`tasks_spped-dial ${this.state.speedDialOpen ? '' : 'not-active'}`}>
                <SpeedDial
                    open={speedDialOpen}
                    ariaLabel="Speed Dial Todos"
                    icon={<SpeedDialIcon />}
                    onClick={() => {
                        this.handleClickSpeedDial();
                        setTimeout(() => {
                            this.state.speedDialOpen && this.handleClickSpeedDial();
                        }, 6000);
                    }}
                    onClose={this.handleClickSpeedDial}
                    /* onBlur={this.handleClickSpeedDial}
                    onFocus={isTouch ? undefined : this.handleClickSpeedDial}
                    onMouseEnter={isTouch ? undefined : this.handleClickSpeedDial}
                    onMouseLeave={this.handleClickSpeedDial} */
                >
                    <SpeedDialAction
                        key="Add new task"
                        icon={<AddIcon />}
                        tooltipTitle="Add new task"
                        onClick={() => {
                            this.addChild.handleToggleDialog();
                        }}
                    />
                    <SpeedDialAction
                        key="Complete all tasks"
                        icon={<EditIcon />}
                        tooltipTitle="Complete all tasks"
                        onClick={() => {
                            this.completeChild.handleToggleDialog();
                        }}
                    />
                    <SpeedDialAction
                        key="Delete all tasks"
                        icon={<DeleteIcon />}
                        tooltipTitle="Delete all tasks"
                        onClick={() => {
                            this.deleteChild.handleToggleDialog();
                        }}
                    />
                </SpeedDial>


                <TaskForm
                    dialogTitle="Add new task"
                    dialogText="To add new task, please enter task title here. You can always change it by clicking on it."
                    defaultTitleValue=""
                    defaultNoteValue=""
                    buttonConfirmText="Add task"
                    isEdit={false}
                    onReference={(reference) => (this.addChild = reference)}
                    onSubmit={(task) => {
                        dispatch(startAddTask(task));
                    }}
                />

                <TaskDialog
                    dialogTitle="Complete all tasks"
                    contentText="Are you sure you want to complete all tasks?"
                    buttonNoText="No, leave tasks"
                    buttonYesText="Yes, complete all tasks"
                    onReference={(reference) => (this.completeChild = reference)}
                    onClickYes={() => {
                        dispatch(startCompleteAll());
                    }}
                />

                <TaskDialog
                    dialogTitle="Delete all tasks"
                    contentText="Are you sure you want to delete all tasks?"
                    buttonNoText="No, leave tasks"
                    buttonYesText="Yes, delete all tasks"
                    onReference={(reference) => (this.deleteChild = reference)}
                    onClickYes={() => {
                        dispatch(startDeleteAll());
                    }}
                />
            </div>
        );
    }
};


export default connect()(SpeedDialTask);
//───────────────────────────────────-SpeedDialTask