//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import DeleteIcon from '@material-ui/icons/Delete';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import {
    startEditTask,
    startRemoveTask,
    startCompletedTask,
    startUnCompletedTask
} from './../../../actions/tasks.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Task
class Task extends Component {
    constructor(props) {
        super(props);

        const {
            title,
            note,
            createdAt,
            completed,
            deleted
        } = props;

        this.state = {
            resize: undefined,
            isEditable: false,

            title: title,
            note: note,
            createdAt: createdAt,
            completed: completed,
            deleted: deleted,

            error: undefined,
            expanded: false,
        };
    }

    handleChange = (event) => {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        })
    }

    handleToggleExpanded = () => {
        this.setState((state) => ({
            expanded: !state.expanded
        }));
    }

    handleToggleDialog = () => {
        this.setState((state) => ({
            isEditable: !state.isEditable
        }));
    };

    handleToggleCheckBox = () => {
        this.setState((state) => ({
            completed: !state.completed
        }));
    }


    updateDimensions = () => {
        this.setState({
            resize: true
        });
    }

    componentWillMount = () => {
        this.updateDimensions();
    }

    componentDidMount = () => {
        window.addEventListener('resize', this.updateDimensions);
    }

    componentWillUnmount = () => {
        window.removeEventListener('resize', this.updateDimensions);
    }

    render() {
        const { dispatch, id } = this.props;
        const {
            isEditable,
            expanded,
            title,
            note,
            createdAt,
            completed,
            deleted
        } = this.state;

        const changedStringTitle = (string) => {
            const screenWidth = (window.innerWidth || document.body.clientWidth);

            return (screenWidth < 330
                ? `${string.substr(0, 20).trim()}...`
                : (string.length > 45)
                    ? `${string.substr(0, 45).trim()}...`
                    : string);
        };

        return (
            <ExpansionPanel
                name="expanded"
                expanded={expanded}
                onChange={this.handleToggleExpanded}
                className="tasks_task-expansion-panel"
            >
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                >
                    <Typography
                        onDoubleClick={this.handleToggleDialog}
                        className={`${completed ? 'tasks_task-completed' : ''}`}
                    >
                        {title.length > 20
                            ? changedStringTitle(title)
                            : title
                        }
                    </Typography>
                </ExpansionPanelSummary>

                <DeleteIcon
                    color="secondary"
                    onClick={() => dispatch(startRemoveTask({ id }))}
                    className={`tasks_task-delete-icon ${expanded ? 'expanded' : ''}`}
                />
                <Checkbox
                    name="completed"
                    checked={completed}
                    onChange={this.handleToggleCheckBox}
                    onClick={() => {
                        completed
                            ? dispatch(startUnCompletedTask({ id }))
                            : dispatch(startCompletedTask({ id }));
                    }}
                    className={`tasks_task-checkbox ${expanded ? 'expanded' : ''}`}
                />

                <ExpansionPanelDetails>
                    <Typography>{note}</Typography>
                </ExpansionPanelDetails>

                <Dialog
                    open={isEditable}
                    onClose={this.handleToggleDialog}
                    aria-labelledby="tasks_dialog-title"
                >
                    <DialogTitle id="tasks_dialog-title">Edit this task</DialogTitle>
                    <DialogContentText className="tasks_dialog-content-text">
                        To edit this task, please enter new task title here. You can always change it by double-clicking on it.
                    </DialogContentText>
                    <DialogContent>
                        <TextField
                            required
                            autoComplete="off"
                            autoFocus
                            defaultValue={title}
                            label="Task title"
                            type="text"
                            name="title"
                            fullWidth
                            margin="dense"
                            onChange={this.handleChange}
                        />
                        <TextField
                            autoComplete="off"
                            defaultValue={note}
                            label="Task note"
                            type="text"
                            name="note"
                            fullWidth
                            margin="dense"
                            onChange={this.handleChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleToggleDialog}>
                            Cancel
                        </Button>
                        <Button onClick={() => {
                            const onSubmit = (task) => {
                                dispatch(startEditTask(id, task));
                            };

                            if (!title) {
                                this.setState({ error: 'Fill in all the fields' });
                            } else {
                                onSubmit({
                                    title,
                                    note,
                                    createdAt: createdAt.valueOf(),
                                    completed,
                                    deleted
                                });
                            }

                            this.handleToggleDialog();
                        }} color="primary">
                            Save task
                        </Button>
                    </DialogActions>
                </Dialog>
            </ExpansionPanel>
        );
    }
};


export default connect()(Task);
//───────────────────────────────────-Task