//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';

import TaskForm from './../TaskForm/index.js';
import DeleteTask from './../DeleteTask/index.js';

import {
    startEditTask,
    startCompletedTask,
    startUnCompletedTask
} from './../../../actions/tasks.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Task
class Task extends Component {
    constructor(props) {
        super(props);

        const {
            title,
            note,
            createdAt,
            completed,
            deleted
        } = props;

        this.state = {
            title,
            note,
            createdAt,
            completed,
            deleted,

            error: undefined,
            expanded: false
        };
    }

    handleToggleExpanded = () => {
        this.setState((state) => ({
            expanded: !state.expanded
        }));
    }

    handleToggleCheckBox = () => {
        this.setState((state) => ({
            completed: !state.completed
        }));
    }

    childUpdateState = (newState) => {
        this.setState({ ...newState });
    }


    // componentDidUpdate(prevProps) { // React 17
    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.props.completed) !== JSON.stringify(nextProps.completed)) {
            this.setState({
                completed: nextProps.completed
            });
        }
    }

    render() {
        const { dispatch, id } = this.props;
        const {
            expanded,
            title,
            note,
            completed,
        } = this.state;

        const changedStringTitle = (string) => {
            const screenWidth = (window.innerWidth || document.body.clientWidth);

            return (screenWidth < 330
                ? `${string.substr(0, 20).trim()}...`
                : (string.length > 45)
                    ? `${string.substr(0, 45).trim()}...`
                    : string);
        };

        return (
            <ExpansionPanel
                name="expanded"
                expanded={expanded}
                onChange={() => {
                    this.handleToggleExpanded();
                    this.props.onExpanded(id, !this.state.expanded);
                }}
                className="tasks_task-expansion-panel"
            >
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}></ExpansionPanelSummary>
                <Typography
                    onClick={() => {
                        this.child.handleToggleDialog();
                    }}
                    className={`tasks_task-title ${expanded ? 'expanded' : ''} ${completed ? 'completed' : ''}`}
                >
                    {title.length > 20
                        ? changedStringTitle(title)
                        : title
                    }
                </Typography>

                <DeleteTask taskID={id} {...this.state} />

                <Checkbox
                    color="primary"
                    name="completed"
                    checked={completed}
                    onChange={this.handleToggleCheckBox}
                    onClick={() => {
                        completed
                            ? dispatch(startUnCompletedTask({ id }))
                            : dispatch(startCompletedTask({ id }));
                    }}
                    className={`tasks_task-checkbox ${expanded ? 'expanded' : ''}`}
                />

                <ExpansionPanelDetails>
                    <Typography>{note}</Typography>
                </ExpansionPanelDetails>

                <TaskForm
                    dialogTitle="Edit this task"
                    dialogText="To edit this task, please enter new task title here. You can always change it by clicking on it."
                    defaultTitleValue={title}
                    defaultNoteValue={note
                        ? note.length > 1500
                            ? `${note.substr(0, 1500).trim()}...`
                            : note
                        : ''}
                    buttonConfirmText="Save task"
                    isEdit={true}
                    onReference={(reference) => (this.child = reference)}
                    onUpdateParent={(newState) => this.childUpdateState(newState)}
                    onSubmit={(task) => {
                        const { dispatch } = this.props;
                        dispatch(startEditTask(id, task));
                    }}
                    {...this.state}
                />
            </ExpansionPanel>
        );
    }
};


export default connect()(Task);
//───────────────────────────────────-Task