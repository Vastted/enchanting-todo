//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-TaskDialog
class TaskDialog extends Component {
    state = {
        dialogOpen: false
    }

    componentDidMount() {
        this.props.onReference(this);
    }

    componentWillUnmount() {
        this.props.onReference(undefined);
    }

    handleToggleDialog = () => {
        this.setState((state) => ({
            dialogOpen: !state.dialogOpen
        }));
    }


    render() {
        const {
            dialogTitle,
            contentText,

            buttonNoText,
            buttonYesText,

            onClickYes
        } = this.props;

        return (
                <Dialog
                    open={this.state.dialogOpen}
                    onClose={this.handleToggleDialog}
                    aria-labelledby="tasks_alert-dialog_title"
                    aria-describedby="tasks_alert-dialog_description"
                >
                    <DialogTitle id="tasks_alert-dialog_title">{dialogTitle}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="tasks_alert-dialog_description">{contentText}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleToggleDialog}>{buttonNoText}</Button>
                        <Button
                            onClick={() => {
                                onClickYes();
                                this.handleToggleDialog();
                            }}
                            color="secondary"
                        >{buttonYesText}</Button>
                    </DialogActions>
                </Dialog>
        );
    }
};


export default connect()(TaskDialog);
//───────────────────────────────────-TaskDialog