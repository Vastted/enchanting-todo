//───────────────────────────────────-imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-TaskForm
class TaskForm extends Component {
    constructor(props) {
        super(props);

        const {
            open = false,
            isEdit = true,

            title = '',
            note = '',
            createdAt = null,
            completed = false,
            deleted = false
        } = props;

        this.state = {
            open,
            isEdit,

            title,
            note,
            createdAt,
            completed,
            deleted
        }
    }

    componentDidMount() {
        this.props.onReference(this);
    }

    componentWillUnmount() {
        this.props.onReference(undefined);
    }

    handleToggleDialog = () => {
        this.setState((state) => ({
            open: !state.open
        }));
    }

    handleToggleDialogReset = () => {
        !this.state.isEdit && this.setState({
            title: null,
            note: null,
            completed: null,
            deleted: null
        });

        this.handleToggleDialog();
    }

    onSubmit = () => {
        const { title, note, completed, deleted } = this.state;
        const { onSubmit } = this.props;

        if (!title) {
            return false;
        } else {
            const newState = {
                title,
                note,
                createdAt: Date.parse(new Date()),
                completed,
                deleted
            };

            onSubmit(newState);
            this.props.onUpdateParent ? this.props.onUpdateParent(newState) : null;
        }

        this.handleToggleDialogReset();
    }

    handleChange = (event) => {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        })
    }

    render() {
        const {
            dialogTitle,
            dialogText,
            buttonConfirmText
        } = this.props;

        return (
            <Dialog
                open={this.state.open}
                onClose={this.handleToggleDialogReset}
                aria-labelledby="tasks_dialog-title"
                className="tasks_dialog-wrapper"
            >
                <DialogTitle id="tasks_dialog-title">{dialogTitle}</DialogTitle>
                <DialogContentText className="tasks_dialog-content-text">{dialogText}</DialogContentText>
                <DialogContent>
                    <TextField
                        required
                        autoFocus
                        autoComplete="off"
                        defaultValue={this.state.title}
                        label="Task title"
                        type="text"
                        name="title"
                        fullWidth
                        margin="dense"
                        onChange={this.handleChange}
                    />
                    <TextField
                        autoComplete="off"
                        multiline
                        rowsMax="5"
                        defaultValue={this.state.note}
                        label="Task note"
                        type="text"
                        name="note"
                        fullWidth
                        margin="dense"
                        onChange={this.handleChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleToggleDialogReset}>
                        Cancel
                    </Button>
                    <Button onClick={this.onSubmit} color="primary">{buttonConfirmText}</Button>
                </DialogActions>
            </Dialog>
        );
    }
};


export default connect()(TaskForm);
//───────────────────────────────────-TaskForm