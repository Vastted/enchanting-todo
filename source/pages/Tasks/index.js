//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';

import CustomAppBar from './../../components/common/CustomAppBar/index.js';

import SpeedDialTask from './SpeedDialTask/index.js';
// import AddTask from './AddTask.[Deprecated]/index.js';
import SortBy from './SortBy/index.js';
import Filter from './Filter/index.js';
import Pagination from './Pagination/index.js';

import './styles.css';
//───────────────────────────────────-imports



//───────────────────────────────────-Tasks
const Tasks = () => (
    <div className="tasks-page_container">
        {/* <CustomAppBar title={`Tasks for ${userEmail}`} /> */}
        <CustomAppBar title={`Tasks`} />

        <div className="tasks-page_wrapper">
            {/* <AddTask /> */}
            <SortBy />
            <Filter />
            <Pagination />
        </div>

        <SpeedDialTask />
    </div>
);


export default connect()(Tasks);
//───────────────────────────────────-Tasks