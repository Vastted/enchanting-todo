//───────────────────────────────────-
export default (state = {}, action) => {
    switch (action.type) {
        case 'SIGN_IN':
            return {
                uid: action.uid,
                email: action.email
            };
        case 'SIGN_OUT':
            return {};
        default:
            return state;
    }
};
//───────────────────────────────────-