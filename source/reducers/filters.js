//───────────────────────────────────-
const filtersReducerDefaultState = {
    text: '',
    sortBy: 'date',
    show: 'All'
};


export default (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text
            };
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'date'
            };
        case 'SORT_BY_TITLE':
            return {
                ...state,
                sortBy: 'title'
            };
        case 'SET_FILTER':
            return {
                ...state,
                show: action.by
            };
        default:
            return state;
    }
};
//───────────────────────────────────-