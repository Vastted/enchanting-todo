//───────────────────────────────────-imports
import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import createHistory from 'history/createBrowserHistory';

import PrivateRoute from './PrivateRoute.js';
import PublicRoute from './PublicRoute.js';


import Home from './../pages/Home/index.js';

import SignIn from './../pages/Authorization/SignIn/index.js';
import SignUp from './../pages/Authorization/SignUp/index.js';

import Application from './../pages/Application.js';
import NotFound from './../pages/NotFound/index.js';
//───────────────────────────────────-imports



//───────────────────────────────────-ApplicationRouters
export const history = createHistory();

const ApplicationRouters = () => (
    <Router history={history}>
        <div>
            <Switch>
                <PublicRoute path="/" component={Home} exact={true} />

                <PublicRoute path="/signin" component={SignIn} exact={true} />
                <PublicRoute path="/signup" component={SignUp} exact={true} />

                <PrivateRoute path="/todos" component={Application} />
                
                <Route component={NotFound} />
            </Switch>
        </div>
    </Router>
);

export default ApplicationRouters;
//───────────────────────────────────-ApplicationRouters