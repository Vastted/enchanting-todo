//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
//───────────────────────────────────-imports



//───────────────────────────────────-PrivateRoute
const PrivateRoute = ({
    isSignIn,
    component: Component,
    ...rest
}) => (
    <Route {...rest} component={(props) => (
        isSignIn ? (
            <div className="page_container">
                <Component {...props} />
            </div>
        ) : (
            <Redirect to="/" />
        )
    )} />
);

const mapStateToProps = (state) => ({
    isSignIn: !!state.authorization.uid
});


export default connect(mapStateToProps)(PrivateRoute);
//───────────────────────────────────-PrivateRoute