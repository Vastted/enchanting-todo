//───────────────────────────────────-imports
import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
//───────────────────────────────────-imports



//───────────────────────────────────-PublicRoute
const PublicRoute = ({
    isSignIn,
    component: Component,
    ...rest
}) => (
    <Route {...rest} component={(props) => (
        isSignIn ? (
            <Redirect to="/todos" />
        ) : (
            <Component {...props} />
        )
    )} />
);

const mapStateToProps = (state) => ({
    isSignIn: !!state.authorization.uid
});


export default connect(mapStateToProps)(PublicRoute);
//───────────────────────────────────-PublicRoute