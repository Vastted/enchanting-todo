//───────────────────────────────────-imports
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import taskReducer from './../reducers/tasks.js';
import filtersReducer from './../reducers/filters.js';
import authorizationReducer from './../reducers/authorization.js';
//───────────────────────────────────-imports



//───────────────────────────────────-store
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


export default () => {
    const store = createStore(
        combineReducers({
            tasksToDo: taskReducer,
            filters: filtersReducer,
            authorization: authorizationReducer
        }),

        compose(applyMiddleware(thunk))
        // composeEnhancers(applyMiddleware(thunk))
    );

    return store;
};
//───────────────────────────────────-store